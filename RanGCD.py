#goal 1: generate vaild keys:e,n
#step 1: choose 2 random prime numbers (p,q):
#step 2: n= p*q
#step 3: find another random int e
#such that gcd(e,(p-1)(q-1))=1
import random
#libary to get me the random prime number 
import sympy as sp

#function to get the GCD using the euclidean algorithm 
def GCDEuclidean(a,b):
    #string
    #print("Here are the intial values:\nx=",a)
    #string
   # print("y=",b)
    #while loop, keeping doing this until mod is 0
    while True:
        #geting the value of a mod b and storing it in c
        c= a % b
        #if c = 0 then break while loop
        if c== 0:
            break 
        #else c is not 0 then make the a value equal to b  
        # and make the b value equal to c. essentially swapping until c = 0
        else: 
            a=b
            b=c
    #print the GCD which would be the last b when c hit 0
    #print("\nGCD=",b)
    return b

def generateValidKeys():
    #keep repeating until i forsure have two prime numbers
    while True:
    #sp.randprime() finds a random prime number in the range
        p=sp.randprime(0,500)
        q=sp.randprime(0,500)
        #check if relatively prime, even though i know it is because of sp.randprime()
        checkIfPrme=GCDEuclidean(p,q)
        #if gcd is 1 then break
        if checkIfPrme==1:
            break
    #getting n
    n=p*q
    #second number for the gcd of e and this one being created
    secondNumber=(1-p)*(1-q)
    #repeat until e makes the gcd =1 with the second number created
    while True:
        #random e
        e=random.randint(1,500)
        #checking gcd
        gcd=GCDEuclidean(e,secondNumber)
        #if gcd is one then break
        if gcd==1:
            break

    print("Gcd(",e,",",secondNumber,")=",gcd,"\nE:",e, "\nN:", n ,"\nP:",p,"\nQ:",q ,"\n(1-p)*(1-q):",secondNumber)

generateValidKeys()

"""
Source:
https://www.geeksforgeeks.org/python-sympy-randprime-method/
describes how to use sympy.randprime

"""